<?php

chdir(__DIR__);
include_once '../lib/init-cli.php';

$content =
    "# auto-generated\n\n" .
    "Options -Indexes\n\n" .
    "<FilesMatch \"\.(css|jpg|js|png|svg|ttf)$\">\n" .
    "    Header set Cache-Control \"public, max-age=31536000\"\n" .
    "</FilesMatch>\n\n" .
    "<FilesMatch \"\.(css|js|svg)$\">\n" .
    "    SetOutputFilter DEFLATE\n" .
    "</FilesMatch>\n\n" .
    'ErrorDocument 403 ' . site_base() . "403.php\n" .
    'ErrorDocument 404 ' . site_base() . "404.php\n\n" .
    "RewriteEngine On\n\n" .
    "RewriteCond %{REQUEST_FILENAME} !-d\n" .
    "RewriteCond %{REQUEST_FILENAME} !-f\n" .
    "RewriteRule ^([^/_]+)/$ _id/?id=$1 [L]\n";

file_put_contents('../.htaccess', $content);

echo "Done\n";
