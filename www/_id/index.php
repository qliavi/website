<?php

include_once '../lib/init-web.php';

$id = request_string('id');
if (!array_key_exists($id, apps())) ErrorPage\notFound();

$app = apps()[$id];

EchoHtml\index([
    'title' => $app['name'],
    'head' =>
        join(array_map(function ($size) use ($id) {
            return '<link rel="icon"' .
                " href=\"https://$id.qliavi.com/images/icons/$size.png\"" .
                " sizes=\"{$size}x{$size}\" />";
        }, [16, 32, 256])),
    'body' =>
        '<div class="IdPage">' .
            '<img class="IdPage-logo"' .
            " src=\"https://$id.qliavi.com/images/icons/128.png\" />" .
            '<h1 class="IdPage-title">' .
                htmlspecialchars($app['name']) .
            '</h1>' .
            '<div>' .
                htmlspecialchars($app['subtitle']) .
            '</div>' .
            '<a class="IdPage-button"' .
            " href=\"https://$id.qliavi.com/\">" .
                'Launch' .
            '</a>' .
            '<h2 class="IdPage-subtitle">Description</h2>' .
            '<div class="IdPage-description">' .
                htmlspecialchars($app['description']) .
                '<br />' .
                '<br />' .
                '<div style="color: grey">' .
                    'Source code is available on ' .
                    "<a class=\"IdPage-link\" href=\"https://gitlab.com/qliavi/$id/\">GitLab</a>." .
                    '<br />' .
                    'Licensed under AGPL (version 3 or later).' .
                '</div>' .
            '</div>' .
            '<div>' .
                '<br />' .
                '<a class="IdPage-link" href="' . site_base() . '">' .
                    'qliavi.com' .
                '</a>' .
            '</div>' .
        '</div>',
]);
