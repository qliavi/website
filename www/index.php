<?php

include_once 'lib/init-web.php';

EchoHtml\index([
    'title' => 'Free and Open-source Web-based Games and Apps',
    'head' =>
        join(array_map(function ($size) {
            return '<link rel="icon"' .
                ' href="' . resource_url("img/icons/$size.png") . '"' .
                " sizes=\"{$size}x{$size}\" />";
        }, [16, 32, 256])),
    'body' =>
        '<div class="Main">' .
            '<div class="Main-top">' .
                '<img class="Main-top-logo"' .
                ' src="img/icons/256.png" />' .
                '<h1 class="Main-top-title">' .
                    'Qliavi' .
                '</h1>' .
            '</div>' .
            '<div class="Main-content">' .
                '<div class="Main-items">' .
                    join(array_map_index(function ($app, $id) {
                        return
                            "<a class=\"Item\" href=\"$id/\">" .
                                '<img class="Item-img"' .
                                " src=\"https://$id.qliavi.com/images/icons/128.png\" />" .
                                '<div>' .
                                    htmlspecialchars($app['name']) .
                                '</div>' .
                            '</a>';
                    }, apps())) .
                '</div>' .
            '</div>' .
        '</div>',
]);
