<?php

function request_string ($name) {

    if (array_key_exists($name, $_GET)) {
        $value = $_GET[$name];
        if (is_string($value) &&
            mb_check_encoding($value, 'UTF-8')) return $value;
    }

    if (array_key_exists($name, $_POST)) {
        $value = $_POST[$name];
        if (is_string($value) &&
            mb_check_encoding($value, 'UTF-8')) return $value;
    }

    return '';

}
