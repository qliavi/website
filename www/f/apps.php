<?php

function apps () {
    return [
        'bubble-shoot' => [
            'name' => 'Bubble Shoot',
            'subtitle' => 'A bubble shooting game.',
            'description' =>
                'Shoot bubbles. Collect three or more of the same color to break.' .
                ' The more your break the more score you gain.',
        ],
        'touch-paint' => [
            'name' => 'Touch Paint',
            'subtitle' => 'A drawing pad app.',
            'description' =>
                'Touch Paint is an app to draw pictures with fingers.' .
                ' Its features include: drawing with multiple fingers at once;' .
                ' user-friendly interface; pencil, line, circle, rectangle and bucket tools;' .
                ' adjusting the size of a brush; color palette.',
        ],
        'five-balls' => [
            'name' => 'Five Balls',
            'subtitle' => 'Collect five balls in a row.',
            'description' =>
                'Move balls from one place to another.' .
                ' Collect five or more in horizontal,' .
                ' in vertical or in diagonal to break.' .
                ' The more your break the more score you gain.',
        ],
        'asteroids' => [
            'name' => 'Asteroids',
            'subtitle' => 'A space shooter game.',
            'description' =>
                'Shoot the asteroids before they hit you.' .
                ' Destroy all of them to advance to the next level.' .
                ' Take green stones to repair your plane.' .
                ' Take red stones to upgrade your weapon.',
        ],
        'speedometer' => [
            'name' => 'Speedometer',
            'subtitle' => 'A GPS tracker app.',
            'description' =>
                'Speedometer uses GPS to show the speed.' .
                ' It also shows how long and how far you went,' .
                ' what were your maximum and average speeds,' .
                ' what is your current altitude, where are you heading to' .
                ' and what where your minimum and maximum altitudes during the trip.' .
                ' These features make it suitable for walking,' .
                ' running, riding a bike or a car.' .
                ' Speedometer supports both imperial and metric unit systems, dark and light themes.',
        ],
        'number-pairs' => [
            'name' => 'Number Pairs',
            'subtitle' => 'Remember where number couples were.',
            'description' =>
                'Tap to open a card and remember its number.' .
                ' Open two cards with the same number to clear them.' .
                ' Clear the board faster with the lowest number of steps to gain higher score.',
        ],
        'tap-tempo' => [
            'name' => 'Tap Tempo',
            'subtitle' => 'A metronome app.',
            'description' =>
                'Tap Tempo is a metronome and an app for measuring time between cycles.' .
                ' It can be used for wide variety of occasions such as:' .
                ' measuring a heart beat, measuring the tempo of a song,' .
                ' measuring the effect interval for guitar effects processors.',
        ],
    ];
}
