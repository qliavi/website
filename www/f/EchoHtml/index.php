<?php

namespace EchoHtml;

function index ($options) {

    $html =
        '<!DOCTYPE html>' .
        '<html>' .
            '<head>' .
                '<title>' .
                    htmlspecialchars($options['title']) . ' | Qliavi' .
                '</title>' .
                '<meta charset="UTF-8" />' .
                '<meta name="viewport" content="width=device-width" />' .
                join(array_map(function ($file) {
                    return '<link rel="stylesheet"' .
                        ' href="' . resource_url($file) . '" />';
                }, [
                    'css/Main.css',
                    'css/IdPage.css',
                    'css/Item.css',
                ])) .
                $options['head'] .
            '</head>' .
            '<body>' .
                $options['body'] .
            '</body>' .
        '</html>';

    ob_start('ob_gzhandler');
    header('Content-Type: text/html; charset=UTF-8');
    die($html);

}
